//
//  ViewController.swift
//  Assignmrnt
//
//  Created by test on 11/04/19.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {
    
    var imageDataArray = [Any]()
    var myCollectionView: UICollectionView!
    private let refreshControl = UIRefreshControl()
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.ipadWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        } else {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.iphoneWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        }
        return layout
    }()
    
    let viewModel = PhotoViewModel(dataService: DataService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpCollectionView()
        self.getPhotoDataFromAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setUpCollectionView() {
        myCollectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)

        myCollectionView.backgroundColor = UIColor.green
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        myCollectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        myCollectionView.backgroundColor = UIColor.white
        myCollectionView.collectionViewLayout = layout
        myCollectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(myCollectionView)
        
        myCollectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        myCollectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        myCollectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        myCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        // Add Refresh Control to Collection View
        if #available(iOS 10.0, *) {
            myCollectionView.refreshControl = refreshControl
        } else {
            myCollectionView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshImgeData(_:)), for: .valueChanged)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Image Data ...") }
    
    @objc private func refreshImgeData(_ sender: Any) {
        getPhotoDataFromAPI()
        self.refreshControl.endRefreshing()
    }
    fileprivate func getPhotoDataFromAPI() {
        viewModel.fetchPhotoDataFromAPI()
        viewModel.didFinishFetch = {
            self.imageDataArray = self.viewModel.imageDataArray
            self.navigationItem.title = self.viewModel.navigationBarTitle
            DispatchQueue.main.async {
                self.myCollectionView.reloadData() }
        }
        viewModel.didFailWithError = {
            if let errorMessage = self.viewModel.errorMesage {
                self.alert(message: errorMessage)
            }
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CollectionViewCell else {
            fatalError("Misconfigured cell")
        }
        
        if let title = (imageDataArray[indexPath.item] as? Row)?.title {
            cell.titleLabel.text = title
        }
        
        if let description = (imageDataArray[indexPath.item] as?Row )?.description {
            cell.descriptionLabel.text = description
        }
        
        if let imageHref = (imageDataArray[indexPath.item] as?Row )!.imageHref {
            let url = URL(string: imageHref)
            cell.customImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeHolder.png"))
        }
          return cell
    }
        
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.ipadWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        } else {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.iphoneWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        }
        super.traitCollectionDidChange(previousTraitCollection)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.ipadWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        } else {
            layout.estimatedItemSize = CGSize(width: Constant.CollectionViewDimentions.iphoneWidth, height: Constant.CollectionViewDimentions.estimatedHeight)
        }
        layout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
}

extension UIViewController {
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
