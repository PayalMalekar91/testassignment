//
//  PhotoViewModel.swift
//  Assignmrnt
//
//  Created by test on 12/04/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
class PhotoViewModel {
    var navigationBarTitle: String?
    var errorMesage: String?
    var imageDataArray = [Any]()
    private var dataService: DataService?

    private var photo: Photo? {
        didSet {
            guard let photoModel = photo else { return}
            setDataToModelProperties(model: photoModel)
            self.didFinishFetch?()
        }
    }
    // MARK: - Closures
    var didFinishFetch: (() -> Void)?
    var didFailWithError: (() -> Void)?

    func setDataToModelProperties(model: Photo) {
        navigationBarTitle = model.title
        imageDataArray = model.rows
    }
    // MARK: - Network Call
    func fetchPhotoDataFromAPI() {
        self.dataService?.requestFetchPhoto(completion: { (photo, error) in
            if let error = error {
                self.errorMesage = error.localizedDescription
                self.didFailWithError?()
                return
            }
            self.photo = photo
        })
    }
    // MARK: - Constructor
    init(dataService: DataService) {
        self.dataService = dataService
    }
}
