//
//  Constant.swift
//  Assignmrnt
//
//  Created by test on 15/04/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    
    struct CollectionViewDimentions {
        static let ipadWidth: CGFloat =  UIScreen.main.bounds.width/2.1
        static let iphoneWidth: CGFloat =  UIScreen.main.bounds.width
        static let estimatedHeight: CGFloat =  200
    }}
