//
//  DataService.swift
//  Assignmrnt
//
//  Created by test on 12/04/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import Alamofire

struct DataService {
    // MARK: - Singleton
    static let shared = DataService()
    // MARK: - URL
    private var dataUrl = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    // MARK: - APIService
    func requestFetchPhoto(completion: @escaping (Photo?, Error?) -> Void) {
        Alamofire.request(self.dataUrl) .validate().responseString { (response) in
            if response.result.isSuccess {
                guard let data = response.value?.data(using: .utf8) else { return }
                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Photo.self, from: data)
                    completion(model, nil)
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil, error)
                }
            } else {
              completion(nil, response.result.error)
            }
        }
    }
}
