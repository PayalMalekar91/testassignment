//
//  Photo.swift
//  Assignmrnt
//
//  Created by test on 12/04/19.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import Alamofire
struct Photo: Codable {
    let title: String
    let rows: [Row]
}

struct Row: Codable {
    let title, description: String?
    let imageHref: String?
}

private func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

private func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers
extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, _, data, error in
            guard error == nil else { return .failure(error!) }
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil,
                                                     completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(),
                        completionHandler: completionHandler)
    }
    @discardableResult
    func responsePhoto(queue: DispatchQueue? = nil,
                       completionHandler: @escaping (DataResponse<Photo>) -> Void) -> Self {
        return responseDecodable(queue: queue,
                                 completionHandler: completionHandler)
    }
}
